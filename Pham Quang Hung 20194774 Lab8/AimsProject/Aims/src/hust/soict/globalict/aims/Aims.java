package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Scanner;
import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.media.factory.*;
import hust.soict.globalict.aims.order.*;
import hust.soict.globalict.aims.utils.*;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application:     ");
		System.out.println("----------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}
	
	public static void showAdminMenu() {
		System.out.println("Product Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new item");
		System.out.println("2. Delete item by id");
		System.out.println("3. Display the items list");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3");
		}
	
	public static void showUserMenu() {
		System.out.println("Welcome to AIMS Store: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Search for an item from the list by title");
		System.out.println("3. Add item to order by id (id in the list of available items of the store");
		System.out.println("4. Remove item from order by id (id in the order)");
		System.out.println("5. Display the order information");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4-5");
		}
	
	public static Media createMedia(MediaCreation mc) {
		return mc.createMediaFromConsole();
		}
	
	public void createMediaItem(ArrayList<Media> mediaItems) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Choose media type (DVD:1, CD:2, Book:3): ");
		int key;
		key = keyboard.nextInt();
		switch (key) {
		case 1:
			mediaItems.add(createMedia(new DVDCreation()));
			System.out.println("Successfull!");
			break;
		case 2:
			mediaItems.add(createMedia(new CDCreation()));
			System.out.println("Successfull!");
			break;
		case 3:
			mediaItems.add(createMedia(new BookCreation()));
			System.out.println("Successfull!");
			break;
		default:
			System.out.println("Error!");
			break;
		}
		}
	
	public static void displayItemList(ArrayList<Media> mediaItems){
		for(int i=0;i<mediaItems.size();i++) {
			System.out.print((i)+". ");
			System.out.println(mediaItems.get(i));
		}
	}
	
	public static void deleteItem(ArrayList<Media> mediaItems,int id) {
		if(id<0||id>=mediaItems.size()) {
			System.out.println("Error!");
			return;
			}
		mediaItems.remove(id);
		System.out.println("Successfull!");
	}
	
	public static void createNewItem(ArrayList<Media> mediaItems) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Choose media type (DVD:1, CD:2, Book:3): ");
		int type;
		System.out.print("Enter your choice: ");
		type = keyboard.nextInt();
		switch (type) {
		case 1:
			mediaItems.add(createMedia(new DVDCreation()));
			System.out.println("Successfull!");
			break;
		case 2:
			mediaItems.add(createMedia(new CDCreation()));
			System.out.println("Successfull!");
			break;
		case 3:
			mediaItems.add(createMedia(new BookCreation()));
			System.out.println("Successfull!");
			break;
		default:
			System.out.println("Error!");
			break;
		}		
	}
	
	public static void Admin(ArrayList<Media> mediaItems) {
		int choice,id;
		Scanner keyboard = new Scanner(System.in);
			do {
			showAdminMenu();
			System.out.print("- Your choice: ");
			choice = keyboard.nextInt();
			switch (choice) {
			case 1:
				createNewItem(mediaItems);
				break;
			case 2:
				System.out.print("Enter Id of the Item: ");
				id = keyboard.nextInt();
				deleteItem(mediaItems, id);
				break;
			case 3:
				displayItemList(mediaItems);
				break;
			case 0:
				break;
			default:
				System.out.println("Error!");
				break;
			}
		    }while(choice!=0);
	}
	
	public static void User(ArrayList<Media> mediaItems) {
		int key;
		Scanner keyboard = new Scanner(System.in);
		Order myOrder=new Order();
		Order newOrder=new Order();
		Media mediaItem;
		int id,kind;
		char ans;
		String title;
		MyDate currentDate;
		do {
			showUserMenu();
			System.out.print("Enter your choice: ");
			key = keyboard.nextInt();
			switch (key) {
			case 1:
				if(newOrder!=null) {
					currentDate=new MyDate();
					currentDate.accept();
					myOrder = newOrder;
					System.out.println("Successful!");
					}
				break;
			case 2:
				keyboard.nextLine();
				System.out.print("Enter the title: ");
				title = keyboard.nextLine();
				for(int i=0; i<mediaItems.size(); i++) {
					if(title.equals(mediaItems.get(i).getTitle())) {
						System.out.print(i+": ");
						System.out.println(mediaItems.get(i));
					}
				}
				break;
			case 3:
				System.out.print("Enter kind of Media(Enter 1 for DVD, 2 for CD, 3 for Book): ");
				kind = keyboard.nextInt();
				System.out.print("Enter the id: ");
				id = keyboard.nextInt();
				if(id<0 || id >=mediaItems.size()) {
					System.out.println("Error");
					break;
				}
				mediaItem = mediaItems.get(id);
				myOrder.addMedia(mediaItem);
				if(kind == Order.MEDIA_CD || kind == Order.MEDIA_DVD) {
				System.out.print("Would you want to play this media(y or n)?: ");
				ans = keyboard.next().charAt(0);
				if(ans == 'y') {
					System.out.println("-----------------------------------");
					if(kind == Order.MEDIA_DVD)
					((DigitalVideoDisc)mediaItem).play();
					else 
						((CompactDisc)mediaItem).play();
					System.out.println("-----------------------------------");
				}
				}
				break;
			case 4:
				System.out.print("- Enter ID of the item: ");
				id = keyboard.nextInt();
				myOrder.removeMediaByID(id);
				break;
			case 5:
				myOrder.printOrder();
				break;
			case 0:
				break;
			default:
				System.out.println("Error!");
				break;
			}
			}while(key!=0);
	}
	public static void main(String[] args) {
		ArrayList<Media> mediaItems = new ArrayList<Media>();
		int key;
		Scanner keyboard = new Scanner(System.in); 
		do {
			showMenu();
			System.out.print("Enter your choice: ");
			key = keyboard.nextInt();
			switch(key) {
			case 1:
				Admin(mediaItems);
			break;
			case 2:
				User(mediaItems);
		    break;
			case 0:
				keyboard.close();
				break;
			default:
				System.out.println("Error");
				break;
			}
	    }while(key!=0);
}
}
