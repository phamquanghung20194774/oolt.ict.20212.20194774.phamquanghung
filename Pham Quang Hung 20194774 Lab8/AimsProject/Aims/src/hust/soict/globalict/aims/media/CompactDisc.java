package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Scanner;

public class CompactDisc extends Disc {
	private String artist;
	private ArrayList<Track> trackliArrayList = new ArrayList<Track>();
	
	public CompactDisc(String title, String category, float cost, int length ,String artist, String director, ArrayList<Track> trackliArrayList) {
		super(title, category, cost, director);
		this.artist = artist;
		this.trackliArrayList = trackliArrayList; 
	}
	
	public CompactDisc(String title, String category, float cost, String artist, String director, ArrayList<Track> trackliArrayList) {
		super(title, category, cost, director);
		this.artist = artist;
		this.trackliArrayList = trackliArrayList;
	}
	
	public CompactDisc(String title, String category, float cost, String director, String artist) {
		super(title, category, cost, director);
		this.artist = artist;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ArrayList<Track> getTrackliArrayList() {
		return trackliArrayList;
	}
	
	public int GetNumTracks() {
		return trackliArrayList.size();
	}

	public void setTrackliArrayList(ArrayList<Track> trackliArrayList) {
		this.trackliArrayList = trackliArrayList;
	}

	public int getTotalLength() {
		int sum=0;
		for (Track i : trackliArrayList){
			sum = sum + i.getLength();
		}
		return sum;
	}
	
	public void displayTrack() {
		int i;
		System.out.println("- Total length: "+getTotalLength());
		for(i=0;i<trackliArrayList.size();i++) {
			System.out.format("Information of Track %d: %s - %f\n",i+1,trackliArrayList.get(i).getTitle(),trackliArrayList.get(i).getLength());
		}
	}
	public String toString() {
		return "CD: "+super.toString()+" Artist: "+artist+" Total Length: "+getTotalLength()+" Cost: "+super.getCost();
	}
	
	
	public void addTrack(Track newTrack) {
		int a=0;
		for(int i=0;i<trackliArrayList.size();i++) {
				if(trackliArrayList.get(i).equals(newTrack)) {
					break;
				}
				else {
					a++;
				}
		}
		if(a==trackliArrayList.size()) {
			trackliArrayList.add(newTrack);
		}
	}
	
	public void removeTrack(Track track) {
		for(int i=0;i<trackliArrayList.size();i++) {
			if(trackliArrayList.get(i).equals(track)) {
				trackliArrayList.remove(i);
				return;
			}
		}
		System.out.println("Track "+track.getTitle()+ " doesn't exist");
		
	}
	
	public void play() {
		int i;
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("Total length: "+getLength());
		for(i=0;i<trackliArrayList.size();i++) {
			trackliArrayList.get(i).play();
		}
	}
	
	public int compareTo(Media o) {
		int numOfTracks=((CompactDisc)o).GetNumTracks();
		if(trackliArrayList.size()>numOfTracks) return 1;
		if(trackliArrayList.size()==numOfTracks) {
			float length_t = ((CompactDisc)o).getLength();
			float length = this.getLength();
			if(length>length_t) return 1;
			if(length==length_t) return 0;
			return -1;
		}
		return -1;
	}

}
