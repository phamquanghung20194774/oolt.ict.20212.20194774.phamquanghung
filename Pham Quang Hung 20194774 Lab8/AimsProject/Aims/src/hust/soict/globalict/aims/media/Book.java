package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Map;
import java.util.TreeMap;
import java.util.Collections;
import java.util.List;

public class Book extends Media {
	private ArrayList<String> authors = new ArrayList<String>();
	String content;
	List<String> contentTokens = new ArrayList<String>();
	Map<String, Integer> wordFrequency;

	public Book() {
		super();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Book(String title, String category, float cost, ArrayList<String> authors) {
		super(title, category, cost);
		this.authors = authors;
	}
	
	public String toString(){
		return "Book "+super.toString()+" [Author: "+authors.toString()+"] [Cost: "+super.getCost()+"]\n[The content length: "+contentTokens.size()+"]\n[The tokens list: "+contentTokens.toString()+"]\n[The word Frequency: "+wordFrequency.toString()+"]";
	}
	
	public void addAuthor(String author) {
		for(String i : authors) {
			if(author.equals(i)) {
				System.out.println("Author "+author+ " exists\n");
				return;
			}
		}
		authors.add(author);
	}
	
	public void removeAuthor(String author) {
		for(String i : authors) {
			if(author.equals(i)) {
				authors.remove(i);
				break;
			}
		}
	}
	
	public void updateAuthor(String aString, String bString) {
		for(String i : authors) {
			if(bString.equals(i)) {
				System.out.println("Author "+bString+ " exists\n");
				return;
			}
		}
		for(int i=0; i<authors.size(); i++) {
			if(aString.equals(authors.get(i))) {
				authors.set(i, bString);
				break;
			}
		}
	}	
	
	public void checkAuthors() {
		for(int i=0; i<authors.size(); i++) {
			for(int j=i+1; j<authors.size(); j++) {
				if(authors.get(i).equals(authors.get(j))) {
					authors.remove(j);
					j--;
				}
			}
		}
	}
	
	public int compareTo(Media obj) {
		return this.getTitle().compareTo(((Book)obj).getTitle());
	}
	
	public void processContent() {
		TreeMap<String, Integer> wordFrequency = new TreeMap<String, Integer>();
		Collections.sort(contentTokens);
		int count;
		for(int i=0;i<contentTokens.size();i++) {
			count =1;
			while(i<contentTokens.size()-1 && contentTokens.get(i).equals(contentTokens.get(i+1))) {i++;count++;}
			wordFrequency.put(contentTokens.get(i), count);
		}
	}
	
}
