package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Scanner;

public class DigitalVideoDisc extends Disc implements Playable {
    public static int max_str=10;
    
	public DigitalVideoDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public boolean search(String title) {
		title = title + " ";
		title = title.toLowerCase();
		String str = new String();
		str = getTitle().toLowerCase();
		char kytu;
		ArrayList<String> aString = new ArrayList<String>();
		int a=0, b=0, d=0;
		for(int i=0; i<title.length(); i++) {
	    	kytu = title.charAt(i);
	    	if(kytu == ' ') {
	    		aString.add(title.substring(b, i));
	    		b=i+1;
	    		a++;
	    	}
	    }
		for(String i : aString) {
			if(str.contains(i)) {
				d++;
			}
		}
		if(d==aString.size()) return true;
		else return false;
	}
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	
	public String toString() {
		return "DigitalVideoDisc: " + super.toString() + ", length: " + length;
	}
	
	public int compareTo(DigitalVideoDisc ob) {
		if(this.getCost()> ob.getCost()) {
			return 1;
		}
		if(this.getCost() < ob.getCost()) {
			return -1;
		}
		return 0;
	}
}

