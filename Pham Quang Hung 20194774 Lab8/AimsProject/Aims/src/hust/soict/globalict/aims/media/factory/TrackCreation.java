package hust.soict.globalict.aims.media.factory;

import java.util.Scanner;
import hust.soict.globalict.aims.media.Track;

public class TrackCreation { 
	public static Track createTrackFromConsole() {
			Scanner keyboard = new Scanner(System.in);
			System.out.print("- Input track title: ");
			String title = keyboard.nextLine();
			System.out.print("- Track Length: ");
			int length = keyboard.nextInt();
			return new Track(title, length);
}
}
