package hust.soict.globalict.test;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.factory.BookCreation;
import java.util.Scanner;

public class BookTest {
	
	public static void showMenu() {
		System.out.println("------------BOOK TEST------------\n");
		System.out.println("1. Enter new book.               \n");
		System.out.println("2. Set/Update content of Book    \n");
		System.out.println("3. Display information of Book   \n");
		System.out.println("0. Exit                          \n");
		System.out.println("---------------------------------\n");
	}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			int key;
			Book book=null;
			String content;
			Scanner keyboard = new Scanner(System.in);
			do {
				showMenu();
				System.out.print("Enter the choice: ");
				key = keyboard.nextInt();
				switch(key) {
				case 1:
					book = (Book)(new BookCreation().createMediaFromConsole());
					break;
				case 2:
					System.out.println("Enter content below: ");
					keyboard.nextLine();
					content = keyboard.nextLine();
					book.setContent(content);
					break;
				case 3:
					System.out.println(book);
					break;
				case 0:
					break;
				default:
					System.out.println("Error!");
					break;
				}
			}while(key!=0);
		}
}
