import java.util.Scanner;

public class lab2_57 {
    public static void main(String args[]) {
    Scanner keyboard = new Scanner(System.in);
    int n, m, i, j;
    System.out.println("Input the number of rows: ");
    n = keyboard.nextInt();
    System.out.println("Input the number of columns: ");
    m = keyboard.nextInt();
    float[][] arr1 = new float[n][m];
    float[][] arr2 = new float[n][m];
    float[][] arr3 = new float[n][m];
    System.out.println("Input array 1:\n");
    for(i=0; i<n; i++) {
        for(j=0; j<m; j++) {
           arr1[i][j] = keyboard.nextFloat();
        }
    }
    System.out.print("Input array 2:\n");
    for(i=0; i<n; i++) {
        for(j=0; j<m; j++) {
           arr2[i][j] = keyboard.nextFloat();
        }
    }
    for(i=0; i<n; i++) {
        for(j=0; j<m; j++) {
           arr3[i][j] = arr1[i][j] + arr2[i][j];
        }
    }
    for(i=0; i<n; i++) {
        for(j=0; j<m; j++) {
            System.out.print(arr3[i][j] + " ");
        }
        System.out.print("\n");
    }
}
}