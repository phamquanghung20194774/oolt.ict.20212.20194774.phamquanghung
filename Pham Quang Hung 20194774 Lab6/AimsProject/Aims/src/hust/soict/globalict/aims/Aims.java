package hust.soict.globalict.aims;

import java.time.LocalDate;
import java.util.Scanner;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.*;
import hust.soict.globalict.aims.utils.*;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}
	
	public static void main(String[] args) {
			showMenu();
		 	int key, choosemedia, id;
		 	do {
			 	Scanner keyboard = new Scanner(System.in);
				System.out.println("Input key: ");
			 	key = keyboard.nextInt();
			switch (key) {
			case 1: 
				Order anOrder = new Order();
				System.out.println("The order has been created");
				System.out.println("Input key: ");
			 	key = keyboard.nextInt();
			 	do {
				switch (key) {
				case 2:
					System.out.println("Choose media type (DVD:1, CD:2, Book:3): ");
					choosemedia = keyboard.nextInt();
					anOrder.addMedia(anOrder.createMedia(choosemedia));
					break;
				case 3:
					System.out.println("Input the ID: ");
					id = keyboard.nextInt();
					anOrder.removeMediaByID(id);
					break;
				case 4:
					anOrder.printOrder();
				default:
					break;
				}
				System.out.println("Input key: ");
				key = keyboard.nextInt();
			}while(key==2 || key==3 || key==4);
			 	break;
			case 0: break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + key);
			}
			}while (key!=0);
}
}
