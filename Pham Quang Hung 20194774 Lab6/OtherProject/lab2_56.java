import java.util.Scanner;

public class lab2_56{
        // ham sap xep
        public void shellSort(int arr[]) {
            int inner, outer;
            int valueToInsert;
            int interval = 1;
            int elements = arr.length;
            int i = 0;
     
            while (interval <= elements / 3) {
                interval = interval * 3 + 1;
            }
     
            while (interval > 0) {
                for (outer = interval; outer < elements; outer++) {
                    valueToInsert = arr[outer];
                    inner = outer;
     
                    while (inner > interval - 1 && arr[inner - interval] >= valueToInsert) {
                        arr[inner] = arr[inner - interval];
                        inner -= interval;
                    }
     
                    arr[inner] = valueToInsert;
                
                }
     
                interval = (interval - 1) / 3;
                i++;
     
            }
        }
     
        public void display(int arr[]) {
            int i;
            System.out.print("[");
     
            // Duyet qua tat ca phan tu
            for (i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
     
            System.out.print("]\n");
        }

   public static void main(String args[]) {
    Scanner keyboard = new Scanner(System.in);
    System.out.print("Nhap so phan tu cua mang: ");
    int n = keyboard.nextInt();
    int[] arr = new int[n];
    System.out.print("Nhap cac phan tu cua mang: \n");
    for (int i = 0; i < n; i++) {
        System.out.printf("a[%d] = ", i);
        arr[i] = keyboard.nextInt();
    }
    lab2_56 shellSort = new lab2_56();
        System.out.println("Mang du lieu dau vao: ");
        shellSort.display(arr);
        shellSort.shellSort(arr);
        System.out.println("\nMang sau khi da sap xep: ");
        shellSort.display(arr);
    float S=0, V;
    for(int i=0; i<arr.length; i++) {
        S=S+arr[i];
    }
    V = S/arr.length;
    System.out.print("Tong cua mang la: " + S+"\n");
    System.out.print("Trung binh cua mang la: " + V + "\n");
}  
}
