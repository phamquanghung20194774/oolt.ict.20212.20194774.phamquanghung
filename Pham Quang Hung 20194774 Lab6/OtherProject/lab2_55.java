import java.util.Scanner;
public class lab2_55 {
        
  public static void main(String[] strings) {

    Scanner keyboard = new Scanner(System.in);

    int NumberOfDays = 0; 
    String MonthName = "unknown";

    System.out.print("Input a month: ");
    int month = keyboard.nextInt();

    System.out.print("Input a year: ");
    int year = keyboard.nextInt();

    switch (month) {
        case 1:
            MonthName = "January";
            NumberOfDays = 31;
            break;
        case 2:
            MonthName = "February";
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                NumberOfDays = 29;
            } else {
                NumberOfDays = 28;
            }
            break;
        case 3:
            MonthName = "March";
            NumberOfDays = 31;
            break;
        case 4:
            MonthName = "April";
            NumberOfDays = 30;
            break;
        case 5:
            MonthName = "May";
            NumberOfDays = 31;
            break;
        case 6:
            MonthName = "June";
            NumberOfDays = 30;
            break;
        case 7:
            MonthName = "July";
            NumberOfDays = 31;
            break;
        case 8:
            MonthName = "August";
            NumberOfDays = 31;
            break;
        case 9:
            MonthName = "September";
            NumberOfDays = 30;
            break;
        case 10:
            MonthName = "October";
            NumberOfDays = 31;
            break;
        case 11:
            MonthName = "November";
            NumberOfDays = 30;
            break;
        case 12:
            MonthName = "December";
            NumberOfDays = 31;
    }
    System.out.println(MonthName + " " + year + " has " + NumberOfDays + " days\n");
}
}
