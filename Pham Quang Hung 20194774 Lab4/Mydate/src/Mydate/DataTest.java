package Mydate;

import java.security.PublicKey;

public class DataTest {
	 public static final int max_date = 3;

	public static void main(String[] args) throws Exception {
         //3. create an instance of mydate by constructor has one string parameter
         MyDate myDate = new MyDate("Second", "December", "ten ten");
         MyDate myDate1 = new MyDate("Second", "December", "ten ten");
         
         DateUtils dateuntis1 = new DateUtils();
         dateuntis1.compareTwoDate(myDate, myDate1);
         
         System.out.println("**************************************");
         System.out.println("Sort: ");

         MyDate myDate2[] = new MyDate[max_date];
         myDate2[0] = new MyDate("Second", "December", "twenty twenty-one");
         myDate2[1] = new MyDate("Thirty-first", "January", "ten ten");
         myDate2[2] = new MyDate("First", "May", "twenty twenty-one");
         dateuntis1.sortDates(myDate2);
         for(int i=0; i<myDate2.length; i++) {
        	 myDate2[i].print();
         }
	}
}
