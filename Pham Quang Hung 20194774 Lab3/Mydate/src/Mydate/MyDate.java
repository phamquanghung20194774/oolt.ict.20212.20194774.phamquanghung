package Mydate;

import java.util.Scanner;
import java.lang.reflect.UndeclaredThrowableException;
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class MyDate {
    Scanner keyboard = new Scanner(System.in);

    private int day=0;    
    private int month=0; 
    private int year=0;   

    public MyDate() {
      day = LocalDate.now().getDayOfMonth();
      month = LocalDate.now().getMonthValue();
      year = LocalDate.now().getYear();      
    }
    
    public MyDate(int year, int month, int day) {
        this.year = year;   
        this.month = month;
        this.day = day;
    }
    public MyDate(String dateString)throws Exception {
        Date date1=new SimpleDateFormat("dd-MMM-yyyy").parse(dateString);
        this.year = date1.getYear();
        this.month = date1.getMonth();
        this.day = date1.getDay();
    }
    
    public int getDay() {
        return day;
    }
    public void setDay(int day)throws Exception {
    	if(this.year != 0 && this.month!=0) {
        	 switch(this.month)
             {
                 case 4, 6, 9, 11:
                     if(day<=0 || day>30) 
                     {
                    	 throw new Exception("date is not valid");
                     }
                     else {
                    	 this.day = day;
                     }  
                 break;
                 case 1, 3, 5, 7, 8, 10, 12:
                	 if(day<=0 || day>31) 
                     {
                    	 throw new Exception("date is not valid");
                     }
                     else {
                    	 this.day = day;
                     }  
                 break;
                 case 2:
                     if((day>29 || day<=0) && (this.year%400==0 && (this.year%4==0 || this.year%100==0)))
                     {
                    	 throw new Exception("date is not valid");
                     } 
                     if(day>28 || day<0) {
                    	 throw new Exception("date is not valid");
                     }
                     this.day=day;
                     break;
             }
    	}
    	else {
       	 throw new Exception("must enter year and month");
		}
    }
    
    public int getMonth() {
        return month;
    }
    public void setMonth(int month)throws Exception {
    	if(month<=0 || month>12) {
        	throw new Exception("month is not valid");
    	}
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) throws Exception {
        if(year<=0 || year>9999) {
        	throw new Exception("year is not valid");
        }
    	this.year = year;
    }

    public void accept() {
        String date = keyboard.nextLine(); 
        String Value[] = date.split("-");
        year = Integer.parseInt(Value[0]);
        month = Integer.parseInt(Value[1]);
        day = Integer.parseInt(Value[2]);
    }

    public void print() {
        System.out.print("Current date is: ");
        System.out.println(day+ "/" + month + "/" + year);
    }
     
}
