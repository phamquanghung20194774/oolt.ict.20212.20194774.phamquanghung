package hust.soict.globalict.aims.disc;
public class DigitalVideoDisc {
	 private String title;
     private String category;
     private String director;
     private int length;
     private float cost;
     public static int max_str=10;
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public int getLength() {
	return length;
}
public void setLength(int length) {
	this.length = length;
}
public float getCost() {
	return cost;
}
public void setCost(float cost) {
	this.cost = cost;
}
public DigitalVideoDisc(String title) {
	super();
	this.title = title;
}
public DigitalVideoDisc(String title, String category) {
	super();
	this.title = title;
	this.category = category;
}
public DigitalVideoDisc(String title, String category, String director) {
	super();
	this.title = title;
	this.category = category;
	this.director = director;
}
public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
	super();
	this.title = title;
	this.category = category;
	this.director = director;
	this.length = length;
	this.cost = cost;
}

public int sleng(String[] str) {
	int n;
	for(n=0; n<str.length; n++) {
		if(str[n]==null) {
			break;
		}
	}
	return n;
}

public boolean search(String title) {
	title = title + " ";
	title = title.toLowerCase();
	String str = new String();
	str = getTitle().toLowerCase();
	char kytu;
	String aString[] = new String[max_str]; 
	int a=0, b=0, d=0;
	for(int i=0; i<title.length(); i++) {
    	kytu = title.charAt(i);
    	if(kytu == ' ') {
    		aString[a] = title.substring(b, i);
    		b=i+1;
    		a++;
    	}
    }
	for(int i=0; i<sleng(aString); i++) {
		if(str.contains(aString[i])) {
			d++;
		}
	}
	if(d==sleng(aString)) return true;
	else return false;
}
}

