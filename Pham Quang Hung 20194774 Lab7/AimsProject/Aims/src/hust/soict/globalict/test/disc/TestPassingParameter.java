package hust.soict.globalict.test.disc;

import hust.soict.globalict.aims.media.*;

public class TestPassingParameter {
	public static void swap(Object o1, Object o2) {
		Object tmp = o1;
		o1 = o2;
		o2 = tmp;
	}

	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc jungleDVD= new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderalla");
		
		swap(jungleDVD, cinderellaDVD);
		System.out.println("Jungle dvd title: "+jungleDVD.getTitle());
		System.out.println("Cinderella dvd title: "+cinderellaDVD.getTitle());
		
		changeTitle(jungleDVD, cinderellaDVD.getTitle());
		System.out.println("Jungle dvd title:"+jungleDVD.getTitle());
	}
}
