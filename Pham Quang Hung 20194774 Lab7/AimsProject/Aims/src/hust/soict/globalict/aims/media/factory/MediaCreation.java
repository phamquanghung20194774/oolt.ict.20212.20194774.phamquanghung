package hust.soict.globalict.aims.media.factory;

import hust.soict.globalict.aims.media.*;

public interface MediaCreation {
	public Media createMediaFromConsole();
}
