package hust.soict.globalict.aims.utils;

import java.util.Scanner;
import java.lang.reflect.UndeclaredThrowableException;
import java.nio.file.FileSystemNotFoundException;
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class MyDate {
    Scanner keyboard = new Scanner(System.in);

    private int day=0;    
    private int month=0; 
    private int year=0;   

    public MyDate() {
      day = LocalDate.now().getDayOfMonth();
      month = LocalDate.now().getMonthValue();
      year = LocalDate.now().getYear();      
    }
    
    public MyDate(String dayString, String monthString, String yearString) {
    	String[] dayStrings = {"", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh",
	            "Eighth", "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth",
	            "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth","Twentieth",
	            "Twenty-firth","Twenty-second","Twenty-third","Twenty-fourth","Twenty-fifth","Twenty-sixth",
	            "Twenty-seventh","Twenty-eighth","Twenty-ninth","Thirtieth","Thirty-first",};
    	
    	for(int i=0;i<dayStrings.length;i++) {
    		
			if(dayString.equals(dayStrings[i])) {
				day=i;
				break;
			}
		}
    
        this.day = day;
        
        String[] monthStrings = {"", "January", "February", "March", "April", "May", "June", "July",
        		"August", "September", "October", "November", "December",
        };
        
        for(int i=0; i<monthStrings.length; i++) {
        	if(monthString.equals(monthStrings[i])) {
        		month=i;
        		break;
        	}
        }
        this.month = month;
        
        String aString[] = new String[100];
        String bString = new String();
        
        String[] yearStrings = {
        		"Oh", "One",  "Two",  "Three",  "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", 
        		"Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", 
        		"Twenty", "Twenty-one", "Twenty-two", "Twenty-three", "Twenty-four", "Twenty-five", "Twenty-six", "Twenty-seven", "Twenty-eight", "Twenty-nine",
        		"Thirty", "Thirty-one", "Thirty-two", "Thirty-three", "Thirty-four", "Thirty-five", "Thirty-six", "Thirty-seven", "Thirty-eight", 
        		"Thirty-nine", "Forty", "Forty-one", "Forty-two", "Forty-three", "Forty-four", "Forty-five", "Forty-six", "Forty-seven", "Forty-eight", "Forty-nine", 
        		"Fifty", "Fifty-one", "Fifty-two", "Fifty-three", "Fifty-four", "Fifty-five", "Fifty-six", "Fifty-seven", "Fifty-eight", "Fifty-nine", 
        		"Sixty", "Sixty-one", "Sixty-two", "Sixty-three", "Sixty-four", "Sixty-five", "Sixty-six", "Sixty-seven", "Sixty-eight", "Sixty-nine", 
        		"Seventy", "Seventy-one", "Seventy-two", "Seventy-three", "Seventy-four", "Seventy-five", "Seventy-six", "Seventy-seven", "Seventy-eight", "Seventy-nine",
        		"Eighty", "Eighty-one", "Eighty-two", "Eighty-three", "Eighty-four", "Eighty-five", "Eighty-six", "Eighty-seven", "Eighty-eight", "Eighty-nine",
        		"Ninety", "Ninety-one", "Ninety-two", "Ninety-three", "Ninety-four", "Ninety-five", "Ninety-six", "Ninety-seven", "Ninety-eight", "Ninety-nine"
        };
        
        if(yearString.equals("One Thousand")) year=1000;
        else if(yearString.equals("Two Thousand")) year=2000;
        else if(yearString.equals("Three Thousand")) year=3000;
        else if(yearString.equals("Four Thousand")) year=4000;
        else if(yearString.equals("Five Thousand")) year=5000;
        else if(yearString.equals("Six Thousand")) year=6000;
        else if(yearString.equals("Seven Thousand")) year=7000;
        else if(yearString.equals("Eight Thousand")) year=8000;
        else if(yearString.equals("Nine Thousand")) year=9000;

        else {
        yearString = yearString + " ";
        
        char kytu;
        int a=0, b=0;
        for(int i=0; i<yearString.length(); i++) {
        	kytu = yearString.charAt(i);
        	if(kytu == ' ') {
        		aString[a] = yearString.substring(b, i);
        		b=i+1;
        		a++;
        	}
        }
        
        for(int i=0; i<a; i++) {
        	for(int j=0; j<yearStrings.length; j++) {
        		if(yearStrings[j].equals(aString[i])) {
        			bString = bString.concat(Integer.toString(j));
        			break;
        		}
        		else if(aString[i].equals("Thousand") || aString[i].equals("Hundred")) {
        			bString = bString.concat("00");
        			break;
				}
        	}
        }
        
        year = Integer.parseInt(bString);
        }
        
        this.year = year; 
           
    }
    
    public int getDay() {
        return day;
    }
    public void setDay(int day)throws Exception {
    	if(this.year != 0 && this.month!=0) {
        	 switch(this.month)
             {
                 case 4, 6, 9, 11:
                     if(day<=0 || day>30) 
                     {
                    	 throw new Exception("date is not valid");
                     }
                     else {
                    	 this.day = day;
                     }  
                 break;
                 case 1, 3, 5, 7, 8, 10, 12:
                	 if(day<=0 || day>31) 
                     {
                    	 throw new Exception("date is not valid");
                     }
                     else {
                    	 this.day = day;
                     }  
                 break;
                 case 2:
                     if((day>29 || day<=0) && (this.year%400==0 && (this.year%4==0 || this.year%100==0)))
                     {
                    	 throw new Exception("date is not valid");
                     } 
                     if(day>28 || day<0) {
                    	 throw new Exception("date is not valid");
                     }
                     this.day=day;
                     break;
             }
    	}
    	else {
       	 throw new Exception("must enter year and month");
		}
    }
    
    public int getMonth() {
        return month;
    }
    public void setMonth(int month)throws Exception {
    	if(month<=0 || month>12) {
        	throw new Exception("month is not valid");
    	}
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) throws Exception {
        if(year<=0 || year>9999) {
        	throw new Exception("year is not valid");
        }
    	this.year = year;
    }

    public void accept() {
        String date = keyboard.nextLine(); 
        String Value[] = date.split("-");
        year = Integer.parseInt(Value[0]);
        month = Integer.parseInt(Value[1]);
        day = Integer.parseInt(Value[2]);
    }

    public void print() {
        System.out.print("Current date is: \n");
        String[] monthStrings = {"", "January", "February", "March", "April", "May", "June", "July",
        		"August", "September", "October", "November", "December",
        };
        for(int i=0; i<monthStrings.length; i++) {
        	if(this.month==i) {
        		System.out.print(monthStrings[i]);
        	}
        }
        for(int i=0; i<=31; i++) {
        	if(getDay()==1 || getDay()==21 || getDay()==31) {
        		System.out.print(" " + getDay() + "st");
        		break;
        	}
        	else if(getDay()==2 || getDay()==22) {
        		System.out.print(" " + getDay() + "nd");
        		break;
        	}
        	else if(getDay()==3 || getDay()==23) {
        		System.out.print(" " + getDay() + "rd");
        		break;
        	}
        	else {
        		System.out.print(" " + getDay() + "th");
        		break;
        	}
        }
        System.out.println(" " + getYear());
    }  
}
