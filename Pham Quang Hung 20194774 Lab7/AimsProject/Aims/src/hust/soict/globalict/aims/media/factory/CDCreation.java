package hust.soict.globalict.aims.media.factory;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.Track;

public class CDCreation implements MediaCreation {
	public Media createMediaFromConsole() {
	Scanner keyboard = new Scanner(System.in);
	int n;
	ArrayList<Track> trackList = new ArrayList<Track>();
	System.out.print("- Input cd (title, category, artist, director, cost): ");
	System.out.print("- Input the cd title: ");
	String title= keyboard.nextLine();
	System.out.print("- Input the cd category: ");
	String category = keyboard.nextLine();
	System.out.print("- Input the artist: ");
	String artist = keyboard.nextLine();
	System.out.print("- Input the director: ");
	String director = keyboard.nextLine();
	System.out.print("- Input the cost: ");
	float cost = keyboard.nextFloat();
	CompactDisc cDisc = new CompactDisc(title, category, cost, artist, director);
	System.out.print("- Input number of tracks: ");
	n = keyboard.nextInt();
	System.out.println("- Input the information of each track: ");
	for(int i=0;i<n;i++) {
		keyboard = new Scanner(System.in); 
		System.out.println("Track "+(i+1));
		cDisc.addTrack(TrackCreation.createTrackFromConsole());		
	}
	cDisc.length = cDisc.getTotalLength();
	return cDisc;
}
}
