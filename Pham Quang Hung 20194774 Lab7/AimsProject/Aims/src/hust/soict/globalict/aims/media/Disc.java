package hust.soict.globalict.aims.media;

public class Disc extends Media {
	
	public int length;
	private String director;
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public Disc(String title, String category, float cost, int length, String director) {
		super(title, category, cost);
		this.length = length;
		this.director = director;
	}
	
	public Disc(String title, String category, float cost, String director) {
		super(title, category, cost);
		this.director = director;
	}
	
	public Disc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public String toString() {
		return super.toString()+" [Director: "+director+"]";
	}

}
