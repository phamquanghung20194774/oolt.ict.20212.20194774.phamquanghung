package hust.soict.globalict.aims.media;

import java.util.Scanner;

public abstract class Media {
	private int ID=0;
	private String title;
	private String Category;
	private float cost;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		this.Category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public Media(String title) {
		super();
		ID++;
		this.title = title;
	}
	public Media(String title, String category) {
		super();
		ID++;
		this.title = title;
		this.Category = category;
	}
	public Media(String title, String category, float cost) {
		super();
		ID++;
		this.title = title;
		this.Category = category;
		this.cost = cost;
	}
	public Media() {
	}
	
	public String toString() {
		return "title: " + title + ", category: " + Category + ", cost: " + cost;
	}
}
