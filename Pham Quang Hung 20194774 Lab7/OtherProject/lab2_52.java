import java.util.Scanner;

public class lab2_52 {
   public static void main(String args[]) {
	   Scanner keyboard = new Scanner(System.in);
	   
	   System.out.print("What's your name?");
	   String strname = keyboard.nextLine();
	   System.out.print("How old are you?");
	   int  iage = keyboard.nextInt();
	   System.out.print("How tall are you?");
       double dheight = keyboard.nextDouble();
       
       System.out.print("Mr/Ms " + strname + ", " + iage + " years old." + " Your height is " + dheight);
   }
}
